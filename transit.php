<?php

require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Collection;
use Dotenv\Dotenv;

$env = new DotEnv(__DIR__);
$env->load();
$env->required(['SLACK_URL']);

$client = new Client;
$res = $client->request('GET', 'https://transit.yahoo.co.jp/traininfo/area/4/');
$html = (string)$res->getBody();

$crawler = new Crawler($html);
$lines = $crawler->filter('.elmTblLstLine.trouble tr')
    ->each(function($node, $i) {
        $a = $node->filter('a');
        if (!$a->count()) {
            return;
        }
        $trouble = $node->filter('.colTrouble');

        return [
            'line' => $a->text(),
            'trouble' => $trouble->text(),
            'link' => $a->attr('href'),
        ];
    }
);

if (!count($lines)) {
    return;
}

// もうちょい詳細情報
$lines = (new Collection($lines))
    ->filter(function($l) {
        return !!$l;
    })
    ->map(function($l) use ($client) {
        $html = $client->request('GET', $l['link'])
            ->getBody()->__toString();
        $trouble = (new Crawler($html))->filter('.trouble');
        $l['detail'] = $trouble->text();
        return $l;
    }
);

// slackに投下
$slackUrl = getenv('SLACK_URL');
$channel = getenv('SLACK_CHANNEL') ? getenv('SLACK_CHANNEL') : 'random';

$message = [
    'text' => '関東の在来線・私鉄・地下鉄で事故・遅延情報があるみたいですよ(Yahoo調べ)',
    'username' => 'ただのボット',
    'icon_emoji' => ':train:',
    'channel' => sprintf('#%s', $channel),
];
$attachments = $lines->map(function($l, $i) {
    $color = '#' . substr(md5($i), 0, 6);
    $attachment = [
        'color' => $color,
        'title' => $l['line'],
        'title_link' => $l['link'],
        'text' => $l['detail'],
        'footer' => $l['trouble'],
    ];
    return $attachment;
})->values();
$message['attachments'] = $attachments->toArray();

$response = $client->request('POST', $slackUrl, [
    'form_params' => [
        'payload' => json_encode($message),
    ],
]);
