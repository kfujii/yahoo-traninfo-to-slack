# Yahooの関東地方の運行情報ページをパースしてなんかあったらSlackに投げるやつ

何もなければ何もしません。

## 準備

1. `composer install`
1. .envファイルを作って`SLACK_URL`環境変数にIncomingWebhookをセット

## 使い方

1. `php transit.php`

## その他

* .envに`SLACK_CHANNEL`があれば指定のチャンネルにポストします。デフォルトは`random`です
* 現状では関東のURLで決め打ちになってますが、15行目あたりのURLを変更すれば他の地方も対応できます
